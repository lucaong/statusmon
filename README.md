# Statusmon

A DSL for monitoring components of a system and availability of features. It
provides a simple way to declare components and checks to monitor their health,
as well as features and their dependencies on components.

The idea is being able to monitor in near-real-time failures in components, and
assess their impact on features to give a human-friendly view on the system health.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'statusmon'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install statusmon

## Usage

```ruby
require 'statusmon'
require 'net/http'

class MySystem < Statusmon::Base
  # configure where to store data (in this case a local Redis)
  store :redis, host: 'localhost', port: 6379

  # declare components of your system and their health checks
  component 'backend API' do
    # for each component, declare checks to be performed, and how often
    # they should be performed (default is every minute)
    check 'service is up', every: 3.minutes do
      # a check is just a block of code using RSpec expectations to check the
      # health of your component
      response = Net::HTTP.get_response(URI('https://my-website.com/api/ping'))
      expect(response.code).to eq('200')
    end

    # there can be multiple checks for a component. The component is marked as
    # failing if one or more checks are failing
    check 'sessions work' do
      response = Net::HTTP.get_response(URI('https://my-website.com/api/session'))
      expect(response.code).to eq('200')
    end
  end

  component 'search engine' do
    check 'service is up', every: 10.seconds do
      response = Net::HTTP.get_response(URI('https://my-website.com/search?q=foo'))
      expect(response.code).to eq('200')
    end
  end

  on_failure do |component|
  end

  on_healed do |component|
  end
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/statusmon.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
