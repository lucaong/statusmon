require 'active_support/core_ext/numeric/time'
require 'active_support/core_ext/string/inflections'

module Statusmon
  module DSL
    attr_reader :components, :callbacks

    def store(*arguments)
      store, *args = arguments
      case store
      when nil
        fail "No store specified" if @store.nil?
        @store
      when Symbol, String
        klass = "Statusmon::Store::#{store.to_s.camelize}".constantize
        @store = klass.new(*args)
      when Class
        @store = store.new(*args)
      else
        @store = store
      end
    end

    def component(name, &block)
      (@components ||= []) << Component.new(name, block)
    end

    def on_failure(&block)
      @callbacks ||= { failure: [], healed: [] }
      @callbacks[:failure] << block
    end

    def on_healed(&block)
      @callbacks ||= { failure: [], healed: [] }
      @callbacks[:healed] << block
    end
  end

  class Component
    attr_reader :name, :checks

    def initialize(name, block)
      @name = name
      instance_exec(&block)
    end

    def check(name, options = {}, &block)
      (@checks ||= []) << Check.new(name, options, &block)
    end
  end

  class Check
    attr_reader :name, :block, :period

    def initialize(name, options = {}, &block)
      options = { every: 1.minute }.merge(options)
      @name, @block = name, block
      @period       = options[:every]
    end
  end
end
