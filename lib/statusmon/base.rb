require 'statusmon/dsl'
require 'statusmon/scheduler'

module Statusmon
  class Base
    extend DSL

    def self.run_scheduler!
      Scheduler.new(components, @store, @callbacks).run!
    end

    def self.run_dashboard!
      Dashboard.run!({
        base: self,
        show_exceptions: false,
        server: %w[puma thin webrick]
      })
    end

    def self.run!
      pid = Process.fork
      if pid == nil
        run_dashboard!
      else
        Process.detach(pid)
        run_scheduler!
      end
    end
  end
end
