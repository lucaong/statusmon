require 'sinatra/base'
require 'json'

module Statusmon
  class Dashboard < Sinatra::Base
    def base
      settings.base
    end

    def get_status
      components = base.components.map do |component|
        [component.name, component_status(component)]
      end.to_h
      { components: components }
    end

    def component_status(component)
      checks = component.checks.map do |check|
        [check.name, { results: base.store.get_results(component.name, check.name) }]
      end.to_h
      { :status => base.store.get_status(component.name), :checks => checks }
    end

    get '/status' do
      content_type 'application/json'
      get_status.to_json
    end
  end
end
