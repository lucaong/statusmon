require 'redis'

module Statusmon
  module Store
    class Redis
      attr_reader :redis

      def initialize(options = {})
        @prefix = options[:prefix] || '_statusmon'
        @redis  = ::Redis.new(options)
      end

      def overdue?(component, check, now, interval)
        redis_key = key(:due, component, check)
        check_and_reschedule(redis_key, now.to_i, interval.to_i)
      end

      def set_result!(component, check, result)
        redis_key = key(:result, component, check)
        redis.multi do
          redis.lpush(redis_key, to_code(result))
          redis.ltrim(redis_key, 0, 9)
          redis.expire(redis_key, 5.days)
        end
      end

      def get_results(component, check)
        redis_key = key(:result, component, check)
        redis.lrange(redis_key, 0, 9).map { |result_code|
          from_code(result_code)
        }
      end

      def get_status(component)
        redis_key = key(:status, component)
        code = redis.get(redis_key)
        from_code(code)
      end

      def set_status!(component, status)
        redis_key = key(:status, component)
        redis.multi do
          redis.set(redis_key, to_code(status))
          redis.expire(redis_key, 5.days)
        end
      end

      private def key(type, component, check = nil)
        [@prefix, type, component, check].compact.join(':')
      end

      private def to_code(value)
        case value
        when true then 'ok'
        when false then 'ko'
        else 'na'
        end
      end

      private def from_code(code)
        case code
        when 'ok' then true
        when 'ko' then false
        else nil
        end
      end

      CHECK_SCRIPT = <<-LUA
        local tstamp = tonumber(redis.call('get', KEYS[1]) or '0')
        if tstamp <= tonumber(ARGV[1]) then
          tstamp = ARGV[1] + ARGV[2]
          redis.call('set', KEYS[1], tstamp)
          redis.call('expire', KEYS[1], 432000)
          return {1, tstamp}
        else
          return {0, tstamp}
        end
      LUA

      private def check_and_reschedule(key, now, interval)
        n, timestamp = @redis.eval(CHECK_SCRIPT, [key], [now, interval])
        [n > 0, timestamp]
      end
    end
  end
end
