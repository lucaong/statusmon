require 'rspec/expectations'
require 'rspec/matchers'

module Statusmon
  class Scheduler
    attr_reader :checkers, :store, :callbacks

    def initialize(components, store, callbacks)
      @store     = store
      @callbacks = callbacks || { failure: [], healed: [] }
      @checkers  = components.flat_map do |component|
        component.checks.map do |check|
          Checker.new(component, check, store, callbacks)
        end
      end
    end

    def run!
      loop do
        begin
          perform_overdue_checks!
        rescue => exception
          $stdout.puts "#{exception.inspect}\n#{exception.backtrace.join("\n  ")}"
          sleep 5
        end
      end
    end

    def perform_overdue_checks!
      @checkers.select(&:overdue?).map do |checker|
        Thread.new do
          with_exception_handling { checker.perform! }
        end
      end.each(&:join)
    end

    private def with_exception_handling
      yield
    rescue => exception
      $stdout.puts "#{exception.inspect}\n#{exception.backtrace.join("\n  ")}"
    end
  end

  class Checker
    attr_reader :component, :check, :store, :callbacks

    def initialize(component, check, store, callbacks)
      @component, @check, @store, @callbacks = component, check, store, callbacks
      @next_check_overdue = 0
    end

    def overdue?
      if Time.now.to_i >= @next_check_overdue
        overdue, @next_check_overdue = store.overdue?(component.name, check.name, Time.now, check.period)
        overdue
      else
        false
      end
    end

    def perform!
      CheckScope.perform!(check)
      set_result!(true)
      true
    rescue ::RSpec::Expectations::ExpectationNotMetError => _
      set_result!(false)
      false
    rescue => exception
      set_result!(nil)
      raise exception
    end

    private def set_result!(result)
      store.set_result!(component.name, check.name, result)
      component_status = store.get_status(component.name)
      case [component_status, result]
      when [true, false]
        set_component_status!(false)
        callbacks[:failure].each do |cbk| cbk.call(component.name) end
      when [false, true]
        if status_healed?
          set_component_status!(true)
          callbacks[:healed].each do |cbk| cbk.call(component.name) end
        end
      when [nil, true], [nil, false]
        set_component_status!(result)
        if result == false
          callbacks[:failure].each do |cbk| cbk.call(component.name) end
        end
      end
    end

    private def status_healed?
      component.checks.map { |check|
        store.get_results(component.name, check.name).first
      }.compact.all?
    end

    private def set_component_status!(status)
      store.set_status!(component.name, status)
    end
  end

  class CheckScope
    include ::RSpec::Matchers

    def initialize(check)
      @check = check
    end

    def perform!
      instance_exec(&@check.block)
    end

    def self.perform!(check)
      new(check).perform!
    end
  end
end
