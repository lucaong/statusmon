require 'spec_helper'
require 'statusmon/store_examples'

describe Statusmon::Store::Redis do
  redis_store = Statusmon::Store::Redis.new(prefix: 'teststmon', db: 2)
  let(:redis_store) { redis_store }

  def cleanup(redis)
    keys = redis.keys('teststmon:*')
    redis.pipelined do
      keys.each { |key| redis.del(key) }
    end
  end

  around do |example|
    begin
      redis = redis_store.redis
      cleanup(redis)
      example.run
    ensure
      cleanup(redis)
    end
  end

  include_examples 'store', redis_store

  it 'cleans up by expiring the keys it creates after 5 days' do
    redis_store.overdue?('foo', 'bar', Time.now, 10)
    redis_store.set_result!('baz', 'qux', true)
    redis_store.redis.keys('teststmon:*').each do |key|
      expect(redis_store.redis.ttl(key)).to be_within(3.seconds).of(5.days)
    end
  end
end
