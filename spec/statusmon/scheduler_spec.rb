require 'spec_helper'

describe Statusmon do
  let(:store) do
    double(:store, {
      :overdue?      => [true, 0],
      :'set_result!' => nil,
      :get_results   => [],
      :get_status    => true,
      :'set_status!' => nil
    })
  end

  describe Statusmon::Checker do
    let(:failure_callback) { Proc.new { 'oops' } }

    let(:healed_callback) { Proc.new { 'yay' } }

    let(:callbacks) do
      { failure: [failure_callback], healed: [healed_callback] }
    end

    let(:check_block) do
      Proc.new { expect(true).to eq(true) }
    end

    let(:check) do
      Statusmon::Check.new('foo', every: 1.minute, &check_block)
    end

    let(:checker) do
      Statusmon::Checker.new(double(:component, name: 'foo'), check, store, callbacks)
    end

    describe '#overdue?' do
      context 'when store.overdue? returns true' do
        let(:store) do
          double(:store, overdue?: [true, Time.now.to_i + 30])
        end

        it 'returns true' do
          expect(checker.overdue?).to eq(true)
        end

        it 'consider check not overdue until next timestamp' do
          expect(store).to receive(:overdue?).once
          expect(checker.overdue?).to eq(true)
          expect(checker.overdue?).to eq(false)
        end
      end

      context 'when store.overdue? returns false' do
        let(:store) do
          double(:store, overdue?: [false, Time.now.to_i + 30])
        end

        it 'returns false' do
          expect(checker.overdue?).to eq(false)
        end

        it 'consider check not overdue until next timestamp' do
          expect(store).to receive(:overdue?).once
          expect(checker.overdue?).to eq(false)
          expect(checker.overdue?).to eq(false)
        end
      end
    end

    describe '#perform!' do
      it 'executes the check block' do
        called = false
        block = Proc.new { called = true }
        allow(checker.check).to receive(:block).and_return block
        checker.perform!
        expect(called).to be(true)
      end

      it 'calls the block in a ::RSpec::Matchers scope' do
        block = Proc.new { expect(self).to be_a(::RSpec::Matchers) }
        allow(checker.check).to receive(:block).and_return block
        expect(checker.perform!).to eq(true)
      end

      context 'when the check meets expectations' do
        let(:check_block) do
          Proc.new { expect(true).to eq(true) }
        end

        it 'returns true' do
          expect(checker.perform!).to eq(true)
        end

        it 'saves the last result of the check as true' do
          expect(store).to receive(:set_result!)
            .with(checker.component.name, checker.check.name, true)
          checker.perform!
        end

        context 'if previous status was nil' do
          before do
            allow(store).to receive(:get_status).and_return(nil)
          end

          it 'sets the status as true' do
            expect(store).to receive(:set_status!).with(checker.component.name, true)
            checker.perform!
          end

          it 'does not execute callbacks' do
            expect(failure_callback).not_to receive(:call)
            expect(healed_callback).not_to receive(:call)
            checker.perform!
          end
        end

        context 'if previous status was false and all checks are fine' do
          before do
            allow(store).to receive(:get_status).and_return(false)
            allow(checker.component).to receive(:checks).and_return [check, double(:check, name: 'bar')]
            allow(store).to receive(:get_results).and_return [true]
          end

          it 'sets the status as true' do
            expect(store).to receive(:set_status!).with(checker.component.name, true)
            checker.perform!
          end

          it 'executes on_healed callbacks' do
            expect(failure_callback).not_to receive(:call)
            expect(healed_callback).to receive(:call)
            checker.perform!
          end
        end

        context 'if previous status was true or not all checks are fine' do
          before do
            allow(checker.component).to receive(:checks).and_return [check, double(:check, name: 'bar')]
          end

          it 'does not sets the status' do
            allow(store).to receive(:get_status).and_return(false)
            allow(store).to receive(:get_results).and_return [false]
            expect(store).not_to receive(:set_status!)
            checker.perform!

            allow(store).to receive(:get_status).and_return(true)
            allow(store).to receive(:get_results).and_return [true]
            expect(store).not_to receive(:set_status!)
            checker.perform!
          end

          it 'does not execute callbacks' do
            expect(failure_callback).not_to receive(:call)
            expect(healed_callback).not_to receive(:call)
            checker.perform!
          end
        end
      end

      context 'when the check expectation is not met' do
        let(:check_block) do
          Proc.new { expect(true).to eq(false) }
        end

        it 'returns false' do
          expect(checker.perform!).to eq(false)
        end

        it 'saves the last result of the check as true' do
          expect(store).to receive(:set_result!)
            .with(checker.component.name, checker.check.name, false)
          checker.perform!
        end

        context 'if previous status was nil' do
          before do
            allow(store).to receive(:get_status).and_return(nil)
          end

          it 'sets the status as false' do
            expect(store).to receive(:set_status!).with(checker.component.name, false)
            checker.perform!
          end

          it 'executes on_failure callbacks' do
            expect(failure_callback).to receive(:call)
            expect(healed_callback).not_to receive(:call)
            checker.perform!
          end
        end

        context 'if previous status was true' do
          before do
            allow(store).to receive(:get_status).and_return(true)
          end

          it 'sets the status as false' do
            expect(store).to receive(:set_status!).with(checker.component.name, false)
            checker.perform!
          end

          it 'executes on_failure callbacks' do
            expect(failure_callback).to receive(:call)
            expect(healed_callback).not_to receive(:call)
            checker.perform!
          end
        end

        context 'if previous status was false' do
          before do
            allow(store).to receive(:get_status).and_return(false)
          end

          it 'does not sets the status' do
            allow(store).to receive(:get_status).and_return(false)
            expect(store).not_to receive(:set_status!)
            checker.perform!
          end

          it 'does not execute callbacks' do
            expect(failure_callback).not_to receive(:call)
            expect(healed_callback).not_to receive(:call)
            checker.perform!
          end
        end
      end

      context 'when the check raises another exception' do
        let(:check_block) do
          Proc.new { raise 'boom!' }
        end

        it 'saves the last result of the check as nil' do
          expect(store).to receive(:set_result!)
            .with(checker.component.name, checker.check.name, nil)
          expect {
            checker.perform!
          }.to raise_error(RuntimeError)
        end

        it 'does not sets the status' do
          expect(store).not_to receive(:set_status!)
          expect {
            checker.perform!
          }.to raise_error(RuntimeError)
        end

        it 'does not execute callbacks' do
          expect(failure_callback).not_to receive(:call)
          expect(healed_callback).not_to receive(:call)
          expect {
            checker.perform!
          }.to raise_error(RuntimeError)
        end
      end
    end
  end

  describe Statusmon::Scheduler do
    let(:components) do
      [
        Statusmon::Component.new(:backend, -> do
          check 'service is up', every: 3.minutes do
            true
          end

          check 'sessions work', critical: false do
            false
          end
        end),

        Statusmon::Component.new(:search_engine, -> do
          check 'service is up', every: 1.second do
            false
          end
        end)
      ]
    end

    let(:scheduler) { Statusmon::Scheduler.new(components, store, nil) }

    describe 'perform_overdue_checks!' do
      it 'performs all overdue checks' do
        scheduler.checkers.select(&:overdue?).each do |checker|
          expect(checker).to receive(:perform!)
        end
        scheduler.perform_overdue_checks!
      end

      it 'does not perform non-overdue checks' do
        scheduler.checkers.each do |checker|
          allow(checker).to receive(:overdue?).and_return false
          expect(checker).not_to receive(:perform!)
        end
        scheduler.perform_overdue_checks!
      end

      context 'when a check raises an exception' do
        it 'does not blow up, but logs the exception to $stdout' do
          allow(scheduler.checkers.last).to receive(:perform!) { raise 'boom!' }
          expect($stdout).to receive(:puts) do |str|
            expect(str).to match(/boom!/)
          end
          expect { scheduler.perform_overdue_checks! }.not_to raise_error
        end
      end
    end
  end
end
