require 'spec_helper'
require 'active_support/core_ext/numeric/time'

describe Statusmon do
  describe 'DSL' do
    let(:base_class) do
      Class.new(Statusmon::Base) do
        component :backend do
          check 'service is up', every: 3.minutes do
            true
          end

          check 'sessions work' do
            false
          end
        end

        component :search_engine do
          check 'service is up', every: 1.second do
            false
          end
        end

        on_failure do
          'oops'
        end

        on_healed do
          'yay'
        end
      end
    end

    it 'declares components' do
      expect(base_class.components.first).to be_a(Statusmon::Component)
      expect(base_class.components.map(&:name)).to eq([:backend, :search_engine])
    end

    it 'declares checks for components' do
      expect(base_class.components.first.checks.first).to be_a(Statusmon::Check)
      expect(base_class.components.first.checks.map(&:name)).to eq(['service is up', 'sessions work'])
      expect(base_class.components.first.checks.map(&:period)).to eq([3.minutes, 1.minute])
    end

    it 'declares callbacks' do
      expect(base_class.callbacks).not_to be_nil
      expect(base_class.callbacks[:failure].first.call).to eq('oops')
      expect(base_class.callbacks[:healed].first.call).to eq('yay')
    end

    describe '.store' do
      context 'when passed a symbol' do
        Statusmon::Store::ATestClass = Struct.new(:foo, :bar)

        let(:base_class) do
          Class.new(Statusmon::Base) { store :a_test_class, 'foo', 'bar' }
        end

        it 'finds the corresponding class in Statusmon::Store namespace, instantiates it, and sets the store' do
          expect(base_class.store).to be_a(Statusmon::Store::ATestClass)
          expect(base_class.store.foo).to eq('foo')
          expect(base_class.store.bar).to eq('bar')
        end
      end

      context 'when passed a class' do
        let(:some_class) { Struct.new(:foo, :bar) }
        let(:base_class) do
          klass = some_class
          Class.new(Statusmon::Base) { store klass, 'foo', 'bar' }
        end

        it 'instantiates it and sets the store' do
          expect(base_class.store).to be_a(Struct)
          expect(base_class.store.foo).to eq('foo')
          expect(base_class.store.bar).to eq('bar')
        end
      end

      context 'when passed an object' do
        let(:object) { Object.new }

        let(:base_class) do
          obj = object
          Class.new(Statusmon::Base) { store obj }
        end

        it 'sets it as the store' do
          expect(base_class.store).to be(object)
        end
      end
    end
  end
end
