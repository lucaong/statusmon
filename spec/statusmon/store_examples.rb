require 'spec_helper'

RSpec.shared_examples 'store' do |implementation|
  let(:store) { implementation }

  describe :overdue? do
    it 'returns [overdue?, recheck_at] with appropriate values and reschedule check when overdue' do
      every = 10 # seconds
      now   = Time.now
      overdue, recheck_at = store.overdue?('my component', 'my check', now, every)
      expect(overdue).to be(true)
      expect(recheck_at).to eq(now.to_i + every)

      overdue, new_recheck_at = store.overdue?('my component', 'my check', now, every)
      expect(overdue).to be(false)
      expect(new_recheck_at).to eq(recheck_at)

      now = Time.now.to_i + 11
      overdue, new_recheck_at = store.overdue?('my component', 'my check', now, every)
      expect(overdue).to be(true)
      expect(new_recheck_at).to eq(now + every)
    end
  end

  describe :set_result! do
    it 'sets the most recent result to true, false or nil' do
      store.set_result!('my component', 'my check', true)
      store.set_result!('my component', 'my check', false)
      store.set_result!('my component', 'my check', nil)
      last_three_results = store.get_results('my component', 'my check').take(3)
      expect(last_three_results).to eq([nil, false, true])
    end
  end

  describe :get_results do
    it 'returns the most recent results, last in first out' do
      expect(store.get_results('my component', 'my check')).to eq([])
      store.set_result!('my component', 'my check', true)
      store.set_result!('my component', 'my check', false)
      store.set_result!('my component', 'my check', nil)
      expect(store.get_results('my component', 'my check')).to eq([nil, false, true])
    end

    it 'returns maximum 10 results' do
      15.times do
        store.set_result!('my component', 'my check', true)
      end
      expect(store.get_results('my component', 'my check')).to eq([true] * 10)
    end
  end

  describe '#set_status! and #get_status' do
    it 'sets and gets the status of a component' do
      expect(store.get_status('my component')).to be(nil)
      store.set_status!('my component', false)
      expect(store.get_status('my component')).to be(false)
      store.set_status!('my component', true)
      expect(store.get_status('my component')).to be(true)
    end
  end
end
