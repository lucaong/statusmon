require 'spec_helper'
require 'rack/test'
require 'json'

ENV['RACK_ENV'] = 'test'

describe Statusmon::Dashboard do
  let(:store) do
    double(:store, {
      :overdue?      => [true, 0],
      :'set_result!' => nil,
      :get_results   => [true, false],
      :get_status    => true,
      :'set_status!' => nil
    })
  end

  let(:base) do
    mock_store = store

    Class.new(Statusmon::Base) do
      store mock_store

      component 'web' do
        check 'service is up', every: 3.minutes do
          true
        end

        check 'sessions work', critical: false do
          false
        end
      end

      component 'search engine' do
        check 'service is up', every: 1.second do
          false
        end
      end
    end
  end

  let(:dashboard) do
    Statusmon::Dashboard.new!
  end

  let(:app) do
    Statusmon::Dashboard
  end

  let(:expected_status) do
    {
      :components => {
        'web' => {
          :status => true,
          :checks => {
            'service is up' => { results: [true, false] },
            'sessions work' => { results: [true, false] }
          }
        },
        'search engine' => {
          :status => true,
          :checks => {
            'service is up' => { results: [true, false] }
          }
        }
      }
    }
  end

  before do
    Statusmon::Dashboard.set :show_exceptions, false
    Statusmon::Dashboard.set :base, base
  end

  describe :get_status do
    it 'returns a hash representing the status of each component' do
      expect(dashboard.get_status).to eq(expected_status)
    end

    it 'shows component status' do
      allow(store).to receive(:get_status) do |component|
        component != 'web'
      end
      status = dashboard.get_status
      expect(status[:components]['web'][:status]).to eq(false)
      expect(status[:components]['search engine'][:status]).to eq(true)
    end
  end

  describe 'web application' do
    include Rack::Test::Methods

    it 'serves a JSON status report on GET /status' do
      get '/status'
      expect(last_response).to be_ok
      expect(last_response.body).to eq(expected_status.to_json)
    end
  end
end
